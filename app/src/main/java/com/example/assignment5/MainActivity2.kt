package com.example.assignment5

import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.content.Intent.EXTRA_EMAIL
import android.os.Bundle
import android.widget.TextView
import com.example.assignment5.databinding.ActivityMain2Binding


class MainActivity2 : AppCompatActivity() {
    private lateinit var binding: ActivityMain2Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMain2Binding.inflate(layoutInflater)
        setContentView(binding.root)
        // Get the Intent that started this activity and extract the string
        val message = intent.getStringExtra(EXTRA_MESSAGE)

        // Capture the layout's TextView and set the string as its text
        val textView = binding.textView.apply {
            text = message
        }
    }
}