package com.example.assignment5

import android.content.Context
import android.content.Intent
import android.content.Intent.EXTRA_EMAIL
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import com.example.assignment5.databinding.ActivityMainBinding
 const val EXTRA_MESSAGE = "com.example.assignment5.MESSAGE"
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.etEmailText.setOnKeyListener{
                view, keyCode, _ -> handleKeyEvent(view, keyCode)
        }
        binding.etPasswordText.setOnKeyListener{
                view, keyCode, _ -> handleKeyEvent(view, keyCode)
        }
        binding.btnLogIn.setOnClickListener {
            checkFields()
        }
    }
    private fun checkEmail(email: String): Boolean {
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return true
        }
        Toast.makeText(applicationContext, getString(R.string.invalid_email), Toast.LENGTH_SHORT).show()
        return false
    }
    private fun checkFields() {
        if (binding.etEmailText.text.isNullOrEmpty() || binding.etPasswordText.text.isNullOrEmpty()) {
            Toast.makeText(applicationContext, getString(R.string.field_is_empty), Toast.LENGTH_SHORT).show()
        }

        val email = binding.etEmailText.text.toString()
        if (!checkEmail(email)) {
            return
        }
        val message = "$email is your email"
        val intent = Intent(this, MainActivity2::class.java).apply {
            putExtra(EXTRA_MESSAGE, message)
        }
        startActivity(intent)

    }

    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            // Hide the keyboard
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }
        return false
    }
}

